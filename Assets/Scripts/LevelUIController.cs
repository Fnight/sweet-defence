﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUIController : MonoBehaviour
{
    [SerializeField] private LevelModel levelModel;
    [SerializeField] private Text moneyText;
    
    void Start()
    {
        levelModel.MoneyChanged += UpdateMoney;
    }

    private void UpdateMoney(int money)
    {
        moneyText.text = $"Money: {money}";
    }
}