﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour
{
    private int dropMoney { get; set; } = 0;
    public int DropMoney
    {
        get => dropMoney;
        set => dropMoney = value;
    }

    public int Collect()
    {
        Destroy(gameObject);
        return DropMoney;
    }
}
