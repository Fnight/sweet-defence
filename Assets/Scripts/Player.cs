﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Camera Camera;
    public float AttackDamage = 30f;

    private LevelModel levelModel;
    
    // Start is called before the first frame update
    void Start()
    {
        levelModel = GameObject.Find("Level Controller").GetComponent<LevelModel>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)){ // if left button pressed...
            Ray ray = Camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var enemy = hit.collider.gameObject.GetComponent<Enemy>();
                if (enemy != null)
                {
                    enemy.TakeDamage(AttackDamage);
                }
            }
        }

        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var money = hit.collider.gameObject.GetComponent<Money>();
                if (money != null)
                {
                    levelModel.Money += money.Collect();
                }
            }
        }
    }
}
