﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Rigidbody enemyRigidbody;
    [SerializeField] private float speed = 1f;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float attackDelay = .5f;
    [SerializeField] private Transform atackPosition;
    [SerializeField] private float health = 100f;
    [SerializeField] private int dropMoney = 10;
    [SerializeField] private GameObject moneyPrefab;

    private float lastTimeAttack = 0;

    private LevelModel levelMode;

    private void Start()
    {
        levelMode = GameObject.Find("Level Controller").GetComponent<LevelModel>();
    }

    void Update()
    {
        if (levelMode.IsBuildMode || lastTimeAttack + attackDelay > Time.time) return;
        var hits = Physics.RaycastAll(atackPosition.position, atackPosition.forward, 0.2f);
        foreach (var hit in hits)
        {
            var wall = hit.collider.gameObject.GetComponent<Wall>();
            if (wall != null)
            {
                wall.TakeDamage(damage);
                lastTimeAttack = Time.time;
            }
        }
    }

    public void TakeDamage(float damage)
    {
        if (damage <= 0) return;
        Debug.Log($"Enemy take damage: {damage}");
        health -= damage;
        if (health <= 0)
        {
            enemyRigidbody.AddForce(Vector3.up * 200, ForceMode.Force);
            StartCoroutine(Destroy());
        }
    }

    void FixedUpdate()
    {
        if (health <= 0 || levelMode.IsBuildMode) return;
        enemyRigidbody.MovePosition(enemyRigidbody.position + transform.forward * speed * Time.fixedDeltaTime);
    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(2);
        var drop = Instantiate(moneyPrefab, transform.position, Quaternion.identity);
        drop.GetComponent<Money>().DropMoney = dropMoney;
        Destroy(gameObject);
    }
}