﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject Enemy;
    public float SpawnDelay = 1f;
    
    private float lastTimeSpawn = 0f;
    private LevelModel levelMode;

    // Start is called before the first frame update
    void Start()
    {
        levelMode = GameObject.Find("Level Controller").GetComponent<LevelModel>();
    }

    // Update is called once per frame
    void Update()
    {
        if (lastTimeSpawn + SpawnDelay > Time.time || levelMode.IsBuildMode) return;
        var position = transform.position;
        var width = transform.localScale.x - Enemy.transform.localScale.x;
        var height = transform.localScale.z - Enemy.transform.localScale.z;
        var newPosition = new Vector3(Random.Range(position.x - width / 2, position.x + width / 2),
            position.y,
            Random.Range(position.z - height / 2, position.z + height / 2));
        var enemy = Instantiate(Enemy, newPosition, Quaternion.identity);
        lastTimeSpawn = Time.time;
    }
}
