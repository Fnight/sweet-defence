﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildPlatform : MonoBehaviour
{
    [SerializeField] private GameObject PlatformUI;
    [SerializeField] private GameObject Mine;
    [SerializeField] private GameObject RootObject;
    private LevelModel levelModel;

    private void Start()
    {
        levelModel = GameObject.Find("Level Controller").GetComponent<LevelModel>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == gameObject && !EventSystem.current.IsPointerOverGameObject())
            {
                OpenUI();
            }
        }
    }

    private void OpenUI()
    {
        foreach (var buildPlatform in FindObjectsOfType<BuildPlatform>())
        {
            if (buildPlatform.gameObject != gameObject)
            {
                buildPlatform.CloseUI();
            }
        }
        PlatformUI.SetActive(!PlatformUI.activeSelf);
    }

    public void CloseUI()
    {
        PlatformUI.SetActive(false);
    }

    public void BuildMine()
    {
        var mine = Mine.GetComponent<IBuilding>();
        if (levelModel.Money >= mine.BuildingCost)
        {
            mine.Build(transform.position);
            RootObject.SetActive(false);
            levelModel.Money -= mine.BuildingCost;
        }
    }
}