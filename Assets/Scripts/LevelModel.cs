﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelModel : MonoBehaviour
{
    [SerializeField] private GameObject battleCamera;
    [SerializeField] private GameObject castleCamera;

    private int money = 0;
    public event Action<int> MoneyChanged;

    public int Money
    {
        get => money;
        set
        {
            money = value;
            MoneyChanged?.Invoke(value);
        }
    }


    public bool IsBuildMode { get; private set; } = false;

    public void ToBuildMode()
    {
        IsBuildMode = !IsBuildMode;
        SwitchCamera();
    }

    private void SwitchCamera()
    {
        if (IsBuildMode)
        {
            battleCamera.SetActive(false);
            castleCamera.SetActive(true);
        }
        else
        {
            battleCamera.SetActive(true);
            castleCamera.SetActive(false);
        }
    }
    
}