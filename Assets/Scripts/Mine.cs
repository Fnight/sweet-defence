﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour, IBuilding
{
    [SerializeField] private int buildingCost = 0;
    private int level = 0;
    [SerializeField] private int[] upgradeCost = {1, 2, 3};

    private float lastTimeMining = 0f;
    [SerializeField] private float miningDelay = 1f;
    private LevelModel levelModel;
    [SerializeField] private int[] miningCounts = {1, 2, 3};

    public int BuildingCost
    {
        get => buildingCost;
        set => buildingCost = value;
    }

    public void Build(Vector3 position)
    {
        Instantiate(gameObject, position, Quaternion.identity);
    }

    public void Upgrade()
    {
    }

    void Update()
    {
        if (levelModel.IsBuildMode || lastTimeMining + miningDelay > Time.time) return;
        levelModel.Money += miningCounts[level];
        lastTimeMining = Time.time;
    }

    private void Start()
    {
        levelModel = GameObject.Find("Level Controller").GetComponent<LevelModel>();
    }

    public int Level
    {
        get => level;
        set
        {
            level = value;
            LevelChanged?.Invoke(value);
        }
    }

    public event Action<int> LevelChanged;

    public int[] UpgradeCosts
    {
        get => upgradeCost;
        set => upgradeCost = value;
    }
}