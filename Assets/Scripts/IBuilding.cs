﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBuilding
{
    int BuildingCost { get; set; }
    void Build(Vector3 position);
    void Upgrade();
    int Level { get; set; }
    event Action<int> LevelChanged;
    int[] UpgradeCosts { get; set; }
}