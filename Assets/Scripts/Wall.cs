﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public Rigidbody WallRigidbody;
    
    [SerializeField]
    private float health = 100;

    public void TakeDamage(float damage)
    {
        if (damage <= 0) return;
        Debug.Log($"Wall take damage: {damage}");
        health -= damage;
        if (health <= 0)
        {
            WallRigidbody.isKinematic = false;
            WallRigidbody.AddExplosionForce(200, WallRigidbody.position, 20);
        }
    }
}
